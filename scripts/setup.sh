#!/bin/bash

# setup git prompt
if [ ! -d /home/vagrant/.bash-git-prompt ]; then
  git clone https://github.com/magicmonty/bash-git-prompt.git /home/vagrant/.bash-git-prompt
#  echo "source /home/vagrant/.bash-git-prompt/gitprompt.sh" >> /home/vagrant/.bashrc
#  source /home/vagrant/.bashrc
fi

# checkout edison sources
if [ ! -d /home/vagrant/edison ]; then
  git clone https://bitbucket.org/karimsam/edison.git /home/vagrant/edison
  cd /home/vagrant/edison
  ./conf/go.sh
  cd src
  ./edison-src/device-software/setup.sh
  source poky/oe-init-build-env
fi

echo "Now type [bitbake edison-image] and go take a couple cups of coffee....."

