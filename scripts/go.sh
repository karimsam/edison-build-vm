#!/bin/bash

set -e

function usage() {
  echo "  Use go.sh [start|stop]"
}

case "$1" in
  'start')
  vagrant up
  vagrant ssh -- -Y
  ;;
  'stop')
  vagrant halt
  ;;
  "-h" | "--help")
  usage
  ;;
  *)
  echo "Invalid syntax" 
  usage
  ;;
esac
