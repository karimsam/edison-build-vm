#!/bin/bash

set -e

EDISON_MNT=/edison
EDISON_BUILD=/home/vagrant/edison/build/toFlash

# TODO save configuration here!


# we clear previous installation
if [ -d "${EDISON_MNT}" ]; then
  echo "Removing previous installation."
  rm -rf "${EDISON_MNT}"/* 
  rm -rf "${EDISON_MNT}"/.* || echo "It is ok to skip these files"
fi

# copy new installation to target
echo "Installing new image..."
cp -r "${EDISON_BUILD}"/* "${EDISON_MNT}"
echo "done."

# we need to tog to the edison and issue the upgrade command
#sudo -u ${USER} minicom -S ttycmdF

# TODO restore configuration after flash
