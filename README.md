# README #

## This README explains how to setup your edison build ##

* Install Vagrant for your host -> https://www.vagrantup.com/downloads
* clone this repository
* Execute the go script to run/stop the vm located in the scripts directory. i.e.: ./go.sh start
* Once the vagrant vm is booted. Go to /vagrant/scripts and execute the setup script.
* You're done. follow the instructions or see the Documentation located at /home/vagrant/edison/doc .
 